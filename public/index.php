<?php

require '../vendor/autoload.php';

use Pecee\SimpleRouter\SimpleRouter;

/** Load database */
require '../config/database.php';

/** Load external routes file */
require '../routes/web.php';

/** Start the routing */
SimpleRouter::start();
