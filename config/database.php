<?php

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;

$capsule->addConnection([
//    'driver' => getenv( 'DB_DRIVER', 'mysql'),
//    'host' => getenv('DB_HOST', '127.0.0.1'),
//    'port' => getenv('DB_PORT', '3306'),
//    'database' => getenv('DB_DATABASE', 'forge'),
//    'username' => getenv('DB_USERNAME', 'forge'),
//    'password' => getenv('DB_PASSWORD', ''),
    'driver' =>'mysql',
    'host' => 'db',
    'port' => '3306',
    'database' => 'oruga',
    'username' => 'root',
    'password' => 'secret',
    'charset' => 'utf8mb4',
    'collation' => 'utf8mb4_unicode_ci',
    'prefix' => '',
]);

$capsule->bootEloquent();
