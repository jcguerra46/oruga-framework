<?php

use Pecee\SimpleRouter\SimpleRouter;

// Example
// SimpleRouter::get($url, $callback, $settings);
// SimpleRouter::post($url, $callback, $settings);
// SimpleRouter::put($url, $callback, $settings);
// SimpleRouter::patch($url, $callback, $settings);
// SimpleRouter::delete($url, $callback, $settings);
// SimpleRouter::options($url, $callback, $settings);

SimpleRouter::get('/', function() {
    return 'Welcome to Api Oruga Framework';
});

SimpleRouter::group(['namespace' => 'App\Controllers'], function() {

    /** Users routes */
    SimpleRouter::get('/users', 'User\UserController@index');

});




