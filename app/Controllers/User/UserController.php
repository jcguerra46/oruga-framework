<?php

namespace App\Controllers\User;

use App\Models\User;

class UserController
{
    public function index()
    {
        return User::all();
    }
}
